// import React, { Component } from "react";
// import Item_Shoe from "./Item_Shoe";

// class List_Shoe extends Component {
//   renderListShoe = () => {
//     return this.props.list.map((item, index) => {
//       return (
//         <Item_Shoe handleAdd2={this.props.handleAdd1} key={index} data={item} />
//       );
//     });
//   };
//   render() {
//     return (
//       <div>
//         <h2 className="my-2">List Shoe</h2>
//         <div className="row">{this.renderListShoe()}</div>
//       </div>
//     );
//   }
// }

// export default List_Shoe;
// import React, { useState } from "react";
// import Cart_Shoe from "./Cart_Shoe";
// import { data_shoe } from "./data_shoe";
// import Item_Shoe from "./Item_Shoe";

// export default function List_Shoe() {
//   let [shoe, setShoe] = useState(data_shoe);
//   let [add, setAdd] = useState([]);

//   let handleAdd = (dataShoe) => {
//     // console.log(add);
//     let index = add.findIndex((item) => {
//       return item.id === dataShoe.id;
//     });

//     if (index === -1) {
//       let newShoe = { ...dataShoe, soLuong: 1 };
//       add.push(newShoe);
//     } else {
//       add[index].soLuong += 1;
//     }
//   };
//   let renderListShoe = () => {
//     return shoe.map((item, index) => {
//       return <Item_Shoe key={index} data={item} handleAdd={handleAdd} />;
//     });
//   };

//   return (
//     <div>
//       <h2 className="my-3">List_Shoe</h2>
//       <div className="row">{renderListShoe()}</div>
//       <Cart_Shoe cart={add} />
//     </div>
//   );
// }
import React from "react";
import Item_Shoe from "./Item_Shoe";

export default function List_Shoe({ list, handleAdd }) {
  const renderListShoe = () => {
    return list.map((item, index) => {
      return <Item_Shoe data={item} key={index} handleAdd={handleAdd} />;
    });
  };
  return (
    <div>
      <h2 className="my-3">List_Shoe</h2>
      <div className="row">{renderListShoe()}</div>
    </div>
  );
}
