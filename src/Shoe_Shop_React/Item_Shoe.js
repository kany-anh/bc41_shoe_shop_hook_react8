// import React, { Component } from "react";

// class Item_Shoe extends Component {
//   render() {
//     let { name, price, image } = this.props.data;
//     return (
//       <div className="card col-3">
//         <img src={image} className="card-img-top" alt="" />
//         <div className="card-body">
//           <h5 className="card-title">{name}</h5>
//           <p className="card-text">{price}</p>
//           <a
//             onClick={() => {
//               this.props.handleAdd2(this.props.data);
//             }}
//             href="#"
//             className="btn btn-primary"
//           >
//             Add
//           </a>
//         </div>
//       </div>
//     );
//   }
// }

// export default Item_Shoe;
// import React from "react";
// import { Card, Button } from "antd";

// const { Meta } = Card;

// export default function Item_Shoe({ data, handleAdd }) {
//   let { id, name, price, image } = data;
//   return (
//     <Card
//       className="col-3"
//       hoverable
//       style={{
//         width: "100%",
//       }}
//       cover={
//         <img
//           id={id}
//           style={{
//             width: "100%",
//             height: "200px",
//             objectFit: "cover",
//           }}
//           alt="example"
//           src={image}
//         />
//       }
//     >
//       <Meta className="my-2" title={name} />
//       <Meta className="my-2" title={price} />
//       <Button
//         onClick={() => {
//           handleAdd(data);
//         }}
//         type="primary"
//       >
//         Add
//       </Button>
//     </Card>
//   );
// }
import React from "react";
import { Card, Button } from "antd";

const { Meta } = Card;

export default function Item_Shoe({ data, handleAdd }) {
  let { id, name, price, image } = data;
  return (
    <Card
      className="col-3"
      hoverable
      style={{
        width: "100%",
      }}
      cover={
        <img
          id={id}
          style={{
            width: "100%",
            height: "200px",
            objectFit: "cover",
          }}
          alt="example"
          src={image}
        />
      }
    >
      <Meta className="my-2" title={name} />
      <Meta className="my-2" title={price} />
      <Button
        onClick={() => {
          handleAdd(data);
        }}
        type="primary"
      >
        Add
      </Button>
    </Card>
  );
}
