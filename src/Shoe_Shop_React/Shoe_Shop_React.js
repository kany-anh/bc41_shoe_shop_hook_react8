// import React, { Component } from "react";
// import Cart_Shoe from "./Cart_Shoe";
// import { data_shoe } from "./data_shoe";
// import List_Shoe from "./List_Shoe";

// class Shoe_Shop_React extends Component {
//   state = {
//     list: data_shoe,
//     cart: [],
//   };
//   handleAdd = (shoe) => {
//     let cloneCart = [...this.state.cart];
//     let index = cloneCart.findIndex((item) => {
//       return item.id === shoe.id;
//     });
//     if (index == -1) {
//       let newShoe = { ...shoe, soLuong: 1 };
//       cloneCart.push(newShoe);
//     } else {
//       cloneCart[index].soLuong++;
//     }
//     this.setState({
//       cart: cloneCart,
//     });
//   };
//   handleDelete = (idShoe) => {
//     let newCart = this.state.cart.filter((item) => item.id !== idShoe);
//     this.setState({
//       cart: newCart,
//     });
//   };
//   handleGainQuantity = (idShoe, luaChon) => {
//     let cloneCart = [...this.state.cart];
//     let index = cloneCart.findIndex((item) => {
//       return item.id === idShoe;
//     });
//     cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
//     this.setState({
//       cart: cloneCart,
//     });
//   };
//   render() {
//     return (
//       <div>
//         <div className="container">
//           <List_Shoe handleAdd1={this.handleAdd} list={this.state.list} />
//         </div>
//         <Cart_Shoe
//           handleGainQuantity={this.handleGainQuantity}
//           handleDelete={this.handleDelete}
//           cart={this.state.cart}
//         />
//       </div>
//     );
//   }
// }

// export default Shoe_Shop_React;

import { useState } from "react";
import Cart_Shoe from "./Cart_Shoe";
import { data_shoe } from "./data_shoe";
import List_Shoe from "./List_Shoe";

export default function Shoe_Shop_React() {
  let [list] = useState(data_shoe);
  let [cartShoe, setCartShoe] = useState([]);
  const handleAdd = (shoe) => {
    let cloneCart = [...cartShoe];
    let index = cloneCart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    setCartShoe(cloneCart);
  };
  const handleDelete = (idShoe) => {
    let newCart = cartShoe.filter((item) => item.id !== idShoe);
    setCartShoe(newCart);
  };
  const handleGainQuantity = (idShoe, luaChon) => {
    let cloneCart = [...cartShoe];
    let index = cloneCart.findIndex((item) => {
      return item.id === idShoe;
    });
    cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    setCartShoe(cloneCart);
  };
  return (
    <div>
      <div className="container">
        <List_Shoe list={list} handleAdd={handleAdd} />
        <Cart_Shoe
          cartShoe={cartShoe}
          handleDelete={handleDelete}
          handleGainQuantity={handleGainQuantity}
        />
      </div>
    </div>
  );
}
